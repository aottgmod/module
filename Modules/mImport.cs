﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using mItems;
using UnityEngine;

namespace Modules
{
    public class mImport : Photon.MonoBehaviour
    {
        public static mImport instance;
        public static AssetBundle FontsBundle;
        
        public void Start()
        {
            StartCoroutine(LoadFont());
        }

        public IEnumerator LoadFont()
        {
            var assetReq = AssetBundle.CreateFromMemory(File.ReadAllBytes(GlobalItems.AssetsPath + @"Fonts.unity3d"));
            yield return assetReq;
            FontsBundle = assetReq.assetBundle;
        }

        public enum Fonts
        {
            Roboto,
            FiraMono,
            Default
        }

        public static Font getFont(Fonts font)
        {
            if (font.Is(Fonts.Roboto) && FontsBundle.Contains("Roboto"))
                return (Font)FontsBundle.Load("Roboto");
            if (font.Is(Fonts.FiraMono) && FontsBundle.Contains("FiraMono-Regular"))
                return (Font)FontsBundle.Load("FiraMono-Regular");
            return ScriptableObject.CreateInstance<GUISkin>().font;
        }

        public static int FontSize(Fonts font)
        {
            return fontSizes[font];
        }
        
        public static readonly Dictionary<Fonts, int> fontSizes = new Dictionary<Fonts, int>
        {
            { Fonts.Roboto, 14 },
            { Fonts.FiraMono, 11 },
            { Fonts.Default, 14 }
        };
    }
}