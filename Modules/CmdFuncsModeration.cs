﻿using System;
using System.IO;
using UnityEngine;

namespace Modules
{
    class CmdFuncsMod : Photon.MonoBehaviour
    {
        public bool cmd(string[] _params, out string _reason)  //mau: will bridge the commands for moderators
        {
            //placeholder
            int target;
            if (!int.TryParse(_params[0], out target))
                return "Insert a valid digit.".Pass(out _reason);
            if (!PhotonNetwork.player.isMasterClient)
                return "You must be masterclient to use 'kick'.".Pass(out _reason);
            PhotonNetwork.CloseConnection(PhotonPlayer.Find(target));
            return Pass(out _reason);
        }
        
        private static bool Pass(out string reason)
        {
            reason = "";
            return true;
        }
    }
}