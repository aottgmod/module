﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mItems;
using UnityEngine;

namespace Modules
{
    public partial class ModControl
    {
        public static bool playerlist_speedup;

        private void refreshFPS()
        {
            fps = 1f / FPSamount;
        }

        private void resetTrackers()
        {
            //mod me add to ignored list here?
            ModProperties.antiDI.Values.ForEach(x => x = 0);  //130 : 50
            ModProperties.antiOSR.Values.ForEach(x => x = 0);  //200 + (int)PhotonNetwork.room.playerCount * 40 : 200;
            ModProperties.antiRPC.Values.ForEach(x => x = 0);  //100 + PhotonNetwork.room.playerCount * 6 : 50 + PhotonNetwork.room.playerCount * 2;
        }

        public static void checkMessagesAndLogsCount()
        {
            if (InRoomChat.messages.Count > 750)
                InRoomChat.messages.Clear();
        }

        public static void tagEntities()
        {
            /*var objects = GameObject.FindGameObjectsWithTag("Player").ToList();
            objects.AddRange(GameObject.FindGameObjectsWithTag("titan"));*/
            /*var objects = GlobalItems.heroes.Select(x => x.Second.gameObject);
            objects.Add(GlobalItems.titans.Select(x => x.Second.gameObject));*/

            
        }
    }
}