﻿using System.Reflection;
using UnityEngine;

namespace Modules
{
    public class Load : MonoBehaviour// : IPlugin
    {
        public static GameObject module;

        /*public string Name
        {
            get { return "Modules_" + Assembly.LoadFrom("Modules.dll").GetName().Version;
            }
        }*/

        public void Start()//OnImport()
        {
            Init();
        }

        void Init()
        {
            module = new GameObject();
            module.AddComponent<Cmd>();
            module.AddComponent<CmdFuncs>();
            module.AddComponent<Console>();
            module.AddComponent<mGUI>();
            module.AddComponent<mImport>();
            module.AddComponent<ModControl>();
            module.AddComponent<GLDraw>();
            Object.DontDestroyOnLoad(module);
        }
    }
}