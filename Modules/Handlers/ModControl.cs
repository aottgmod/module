﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Modules
{
    partial class ModControl : MonoBehaviour
    {
        private static ModControl instance;

        private const float waitVal = 0.5f;
        private static WaitForSeconds wait;
        private static Coroutine perma;
        private static int mult_counter = 0;

        private static readonly IDictionary<string, IDictionary<int, Coroutine>> staticRoutines = new Dictionary<string, IDictionary<int, Coroutine>>();
        private static IDictionary<string, int> staticIndex = new Dictionary<string, int>();

        public static float fps;
        public static float FPSamount;

        void Start()
        {
            instance = this;
            wait = new WaitForSeconds(waitVal);
            perma = StartCoroutine(permaRoutine());
        }

        public static void add(string name, IEnumerator routine)
        {
            if (!staticRoutines.ContainsKey(name))
            {
                staticRoutines.Add(name, new Dictionary<int, Coroutine> { { 1, instance.StartCoroutine(routine) } });
                staticIndex.Add(name, 1);
            }
            else
                staticRoutines[name].Add(++staticIndex[name], instance.StartCoroutine(routine));
        }

        public static void remove(string name, int index)
        {
            staticRoutines[name].Remove(index);
            staticRoutines.Remove(name);
            staticIndex.Remove(name);
        }

        private IEnumerator permaRoutine()
        {
            while (true)
            {
                refreshFPS();
                if (playerlist_speedup)  //mau: requires the playerlist to refresh more often (will be usefull... maybe......)
                    FengGameManagerMKII.refreshPlayerlist();
                if (mult_counter % (1 / waitVal) == 0)  //do every second
                {
                    resetTrackers();
                }
                if (mult_counter % (5 / waitVal) == 0)  //do every 5 seconds
                {
                    //checkMessagesAndLogsCount();
                    FengGameManagerMKII.refreshPlayerlist();
                }
                if (mult_counter % (10 / waitVal) == 0)  //do every 10 seconds
                {
                    
                }
                yield return wait;
                mult_counter++;
            }
        }
    }
}
