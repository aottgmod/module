﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UnityEngine;
using mItems;
using Application = UnityEngine.Application;
using Screen = UnityEngine.Screen;

namespace Modules
{
    public class Console : Photon.MonoBehaviour
    {
        public static Console instance;

        public struct Log
        {
            public string message;
            public string timestamp;
            public LogType type;
        }

        public static char ignoreChar = '+';
        private static readonly Rect GuiRect = new Rect(Screen.width - 265f, Screen.height - 325f, 265f, 305f);
        public static IDictionary<int, Log> logs = new Dictionary<int, Log>();
        private static bool show; //when '!show' make the gui alpha 0.1f
        private bool showdatetime;

        private GUIStyle style;
        public static Font mFont;

        //private Rect windowRect = new Rect(50, 50, Screen.width - 100, Screen.height - 100);
        private Rect windowRect = new Rect(Screen.width / 2f + 100f, Screen.height / 2f - 100f, 550f, 400f);

        void Start()
        {
            instance = this;
            Yield.Begin(new WaitForSeconds(6f), loadStyle);
        }

        private void loadStyle()
        {
            var f = mImport.Fonts.Roboto;
            mFont = mImport.getFont(f);
            style = new GUIStyle
            {
                margin = new RectOffset(0, 0, 0, 0),
                wordWrap = true,
                font = mFont
            };
            style.fontSize = mImport.FontSize(f);
            style.normal.textColor = Color.white;
        }

        public static void chageFont(mImport.Fonts font)
        {
            instance.style.font = mImport.getFont(font);
            instance.style.fontSize = mImport.FontSize(font);
        }

        public enum LogType
        {
            Message = 0,
            Assert = 1,
            Error = 2,
            Exception = 3,
            Log = 4,
            Warning = 5,
            Info = 6,
            Network = 7,
            Hint = 8,
            Console = 9,
            Mod = 10
        }

        private static string getCol(LogType type, bool bbPHP = false)
        {
            if (!bbPHP)
                return "<color=#" + logTypeColors[type].ToHex(false) + ">";
            return logTypeColors[type].ToHex();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Keypad0))
                show = !show;
        }

        private Color fade(int i)
        {
            float n;
            if (i < 15)
                n = i;
            n = i / 15;
            return new Color(1f, 1f, 1f, i * 0.08f);
        }
        
        private void OnGUI()
        {
            if (style != null && PhotonNetwork.connectionStateDetailed == PeerState.Joined)
            {
                GUILayout.BeginArea(GuiRect);
                GUILayout.FlexibleSpace();
                if (logs.Count < 15)
                {
                    for (var i = 0; i < logs.Count; i++)
                    {
                        //style.normal.textColor = fade(i);
                        GUILayout.Label(logs[i].message, style);
                    }
                }
                else
                {
                    for (var j = logs.Count - 15; j < logs.Count; j++)
                    {
                        //style.normal.textColor = fade(j);
                        GUILayout.Label(logs[j].message, style);
                    }
                }
                //style.font.material.color += new Color(0f, 0f, 0f, 1f);
                GUILayout.EndArea();
            }
            /*if (show)
                windowRect = GUI.Window(11, windowRect, ConsoleWindow, "<b>Console</b>");*/
        }

        private void ConsoleWindow(int windowID)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("DebugLog options: ");
            GUILayout.FlexibleSpace();
            showdatetime = GUILayout.Toggle(showdatetime, "Show datetime");
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            if (logs.Count > 0)
            {
                for (var i = 0; i < logs.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    var log = logs[i];
                    if (showdatetime)
                    {
                        GUILayout.Label(log.timestamp);
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.Label(log.message);
                    GUILayout.FlexibleSpace();
                    GUILayout.Label(log.type.ToString());
                    GUILayout.EndHorizontal();
                }
            }
            /*if (GUILayout.Button("Clear console", GUILayout.Height(35), GUILayout.Width(Screen.width / 2)))
                logs.Clear();*/
        }

        /// <summary>
        /// Add 'ignoreChar' in the beginning of a word or put a sentence between braces {this is an example}
        /// to prevent coloring
        /// </summary>
        public static void addLog(string msg, LogType type = LogType.Log)
        {
            msg = parseString(msg, getCol(type));

            logs.Add(logs.Count + 1, new Log { message = msg, timestamp = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"), type = type });
            if (logs.Count > 100)
                logs.Remove(0);
        }

        public static void addChat(string msg, LogType type = LogType.Info)
        {
            InRoomChat.AddLine(parseString(msg, getCol(type)));
        }

        private static string parseString(string str, string color)
        {
            str = Regex.Replace(str, @"\" + ignoreChar + @"(?<op_sign>[\w\d\/\*\-\+\'\""]+)", "<$1>");
            str = Regex.Replace(str, @"{(?<brace>.+)}", "<$1>");
            var arr = Regex.Split(str, "(<)|(>)");

            for (var i = 0; i < arr.Length - 1; i++)
            {
                if (arr[i] == "<")
                    arr[i] = "</color></b>";
                else if (arr[i] == ">")
                    arr[i] = "<b>" + color;
            }
            str = "<b>" + color + arr.Join("") + "</color></b>";
            //<b><color=#123456>
            return str;
        }

        private static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>
        {
            { LogType.Message, CColor.message },
            { LogType.Assert, CColor.assert },
            { LogType.Error, CColor.error },
            { LogType.Exception, CColor.exception },
            { LogType.Log, CColor.log },
            { LogType.Warning, CColor.warning },
            { LogType.Info, CColor.info },
            { LogType.Network, CColor.network },
            { LogType.Hint, CColor.hint },
            { LogType.Console, CColor.console },
            { LogType.Mod, CColor.mod }
        };
    }
}