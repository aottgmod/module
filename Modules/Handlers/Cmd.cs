﻿using System.Collections;
using System.Collections.Generic;

namespace Modules
{
    public class Cmd : Photon.MonoBehaviour
    {
        public static Cmd instance;
        private static CmdFuncs functions;

        public delegate TResult delegate_call<in in_param, out_param, TResult>(in_param _input, out out_param _output);
        public readonly Dictionary<string, delegate_call<string[], string, bool>> funcs = new Dictionary<string, delegate_call<string[], string, bool>>();

        public IEnumerator command_handler(string input)
        {
            var data = input.Split(' ');                       //data - inputLine
            var command = data[0].Remove(0, 1);                //command - base command (first item from data)
            delegate_call<string[], string, bool> call_method; //call_method - calls the method 'string' with the 'string[]' params and return 'bool' if executed successfully
            if (!funcs.TryGetValue(command, out call_method))  //if command exists in 'funcs' the method of the cmd will pass to 'call_method'
            {                                                  //else you get this error message (command does not exist)
                Console.addChat("Could not find command \"{" + command + "}\"", Console.LogType.Error);
                yield return null;
            }

            string ret_failreason;
            var call_params = data.RemoveAt(0);
            if (call_method != null && !call_method(call_params, out ret_failreason))  //executes the command and if it fails for some reason you get the reason in 'ret_failreason'
                Console.addChat(ret_failreason, Console.LogType.Error);
        }

        void Start()
        {
            instance = this;
            functions = CmdFuncs.instance = new CmdFuncs();  //CmdFuncs is instantiated here to keep us from adding it as component (fix me)
            funcs.Add("mic", functions.mic);
            funcs.Add("testconsole", functions.console_test);
            funcs.Add("log", functions.log);
            funcs.Add("restart", functions.restart);
            funcs.Add("kick", functions.kick);
            funcs.Add("capfps", functions.target_fps);
            funcs.Add("gc", functions.garbage_collector);
            funcs.Add("l", functions.log_this);
            funcs.Add("setfont", functions.load_font);
            funcs.Add("stats", functions.check_stats);
            funcs.Add("test", functions.test_hero);
            funcs.Add("plug", functions.testing);


            //cmds to be moved to 'Moderation'
        }
    }
}