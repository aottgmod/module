﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Modules
{
    class CmdFuncs : Photon.MonoBehaviour
    {
        public static CmdFuncs instance;

        public static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            var rand = Random.Range(0, v.Length);
            return (T)v.GetValue(rand);
        }

        public bool console_test(string[] _params, out string reason)
        {
            for (var i = 1; i <= 15; i++)
            {
                Console.addLog("test0 +test1 test0 {test2 test2 test2} test0", RandomEnumValue<Console.LogType>());
            }
            return Pass(out reason);
        }

        public bool log(string[] _params, out string reason)
        {
            Console.addLog("<color=green>test</color>");
            return Pass(out reason);
        }

        public bool mic(string[] _params, out string reason)
        {
            int id_;
            if (int.TryParse(_params[0], out id_))
            {
                rMicMod.id = Convert.ToInt32(id_);
                return Pass(out reason);
            }
            FengGameManagerMKII.MKII.toggleMic();
            return Pass(out reason);
        }

        public bool restart(string[] _params, out string reason)
        {
            if (!PhotonNetwork.player.isMasterClient)
                return "You must be masterclient.".Pass(out reason);
            FengGameManagerMKII.MKII.restartGame();
            return Pass(out reason);
        }

        public bool kick(string[] _params, out string reason)
        {
            int target;
            if (!int.TryParse(_params[0], out target))
                return "Insert a valid digit.".Pass(out reason);
            if (!PhotonNetwork.player.isMasterClient && !PhotonNetwork.player.isRM)
                return "You must be masterclient/moderator to use 'kick'.".Pass(out reason);
            PhotonNetwork.CloseConnection(PhotonPlayer.Find(target));
            return Pass(out reason);
        }

        public bool target_fps(string[] _params, out string reason)
        {
            int n;
            if (!int.TryParse(_params[0], out n))
                return "Value is not an integer.".Pass(out reason);
            Application.targetFrameRate = _params[0].To<int>();
            return Pass(out reason);
        }

        public bool garbage_collector(string[] _params, out string reason)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return Pass(out reason);
        }

        public bool log_this(string[] _params, out string reason)
        {
            Console.addLog(_params.Join(" "), Console.LogType.Assert);
            return Pass(out reason);
        }

        public bool load_font(string[] _params, out string reason)
        {
            switch (_params[0].ToLower())
            {
                case "roboto":
                    Console.chageFont(mImport.Fonts.Roboto); break;
                case "firamono":
                    Console.chageFont(mImport.Fonts.FiraMono); break;
                case "default":
                case "arial":
                    Console.chageFont(mImport.Fonts.Default); break;
                default:
                    Console.addChat("Theres no font with that name.", Console.LogType.Error); break;
            }
            return Pass(out reason);
        }

        public bool testing(string[] _params, out string reason)
        {
            InRoomChat.AddLine("Contains: +" + kPluginManager.instance.hasPlugin(_params[0]));
            return Pass(out reason);
        }
        
        public bool check_stats(string[] _params, out string reason)
        {
            int target;
            if (!int.TryParse(_params[0], out target))
                return "Insert a valid digit.".Pass(out reason);
            var player = PhotonPlayer.Find(target);
            Console.addChat("Char: +" + player.stats.CHARACTER + "\t\tSkill: +" + player.stats.skill, Console.LogType.Assert);
            Console.addChat("SPD: +" + player.stats.SPD + "\t\tACL: +" + player.stats.ACL);
            Console.addChat("GAS: +" + player.stats.GAS + "\t\tBLA: +" + player.stats.BLA);
            Console.addChat("Total: +" + player.stats.total, player.stats.hasBoost ? Console.LogType.Error : Console.LogType.Info);
            return Pass(out reason);
        }

        public bool test_hero(string[] _params, out string reason)
        {
            foreach (var player in PhotonNetwork.playerList)
                Console.addChat("ID: " + player.ID + " {" + player.hero + "}");
            return Pass(out reason);
        }

        /*public bool kick(string[] _params, out string reason) //feng kick
        {
            int id;
            if (!int.TryParse(_params[0], out id))
                return "This command only accept integers.".Pass(out reason);
            PhotonPlayer target;
            if (!PhotonPlayer.Find(id, out target))
                return "Player not found.".Pass(out reason);
            if (target.isMasterClient)
                return "You can't kick the master client.".Pass(out reason);
            if (target.isLocal)
                return "You can't kick yourself.".Pass(out reason);
            FengGameManagerMKII.MKII.photonView.RPC("Chat", PhotonTargets.All, id, LoginFengKAI.player.name);
            return Pass(out reason);
        }*/

        private static bool Pass(out string reason)
        {
            reason = "";
            return true;
        }
    }

    internal static class Ext
    {
        public static bool Pass(this string val, out string reason)
        {
            reason = val;
            if (val.IsNullOrEmpty())
                return true;
            return false;
        }
    }
}