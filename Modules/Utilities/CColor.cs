﻿using System;
using UnityEngine;

namespace Modules
{
    class CColor
    {
        //http://forum.unity3d.com/threads/gamma-vs-linear-space.134191/
        //http://wiki.unity3d.com/index.php?title=Fade

        public static Color log { get { return hexToRGB("A8FF24"); } }
        public static Color doge { get { return hexToRGB("FF8000"); } }  //FF8000
        public static Color assert { get { return hexToRGB("FF8400"); } }
        public static Color rc { get { return hexToRGB("5A5AFF"); } }
        public static Color warning { get { return hexToRGB("FFFF00"); } }
        public static Color info { get { return hexToRGB("FFCC00"); } }
        public static Color error { get { return hexToRGB("BE0000"); } }
        public static Color hint { get { return hexToRGB("01CD00"); } }
        public static Color console { get { return hexToRGB("0000FF"); } }
        public static Color network { get { return hexToRGB("4294F9"); } }
        public static Color exception { get { return hexToRGB("FF00AA"); } }  //AA70FF
        public static Color message { get { return hexToRGB("D7D7D7"); } }
        public static Color mod { get { return hexToRGB("892EFE"); } }

        public static Color hexToRGB(string hex)
        {
            if (hex.StartsWith("#"))
                hex = hex.Remove(0, 1);

            var bigint = Convert.ToInt32(hex, 16);
            var r = (bigint >> 16) & 255;
            var g = (bigint >> 8) & 255;
            var b = bigint & 255;

            return new Color(r, g, b);
        }
    }
}