﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Modules
{
    internal class ChatExtended
    {
        [Obsolete("Use .FixChat(string)", true)]
        public static string fixChat(string input)
        {
            var rgx = new Regex(@"(?<cs><color=.*?>)|(?<ce><\/color>)|(?<ss><size=.*?>)|(?<se><\/size>)|(?<bs><b>)|(?<be><\/b>)|(?<is><i>)|(?<ie><\/i>)");
            var tags = rgx.Matches(input);

            int colStart, colEnd, sizeStart, sizeEnd, bStart, bEnd, iStart, iEnd;
            colStart = colEnd = sizeStart = sizeEnd = bStart = bEnd = iStart = iEnd = 0;
            for (var i = 0; i < tags.Count; i++)
            {
                colStart += tags[i].Groups["cs"].Captures.Count;
                colEnd += tags[i].Groups["ce"].Captures.Count;
                sizeStart += tags[i].Groups["ss"].Captures.Count;
                sizeEnd += tags[i].Groups["se"].Captures.Count;
                bStart += tags[i].Groups["bs"].Captures.Count;
                bEnd += tags[i].Groups["be"].Captures.Count;
                iStart += tags[i].Groups["is"].Captures.Count;
                iEnd += tags[i].Groups["ie"].Captures.Count;
            }
            var cdiff = colEnd - colStart;
            var sdiff = sizeEnd - sizeStart;
            var bdiff = bEnd - bStart;
            var idiff = iEnd - iStart;
            if (cdiff < 0)
                input += string.Concat(Enumerable.Repeat("</color>", colStart - colEnd));
            else
                input = input.RemoveCount("</color>", cdiff);
            if (sdiff < 0)
                input += string.Concat(Enumerable.Repeat("</size>", sizeStart - sizeEnd));
            else
                input = input.RemoveCount("</size>", sdiff);
            if (bdiff < 0)
                input += string.Concat(Enumerable.Repeat("</b>", bStart - bEnd));
            else
                input = input.RemoveCount("</b>", bdiff);
            if (idiff < 0)
                input += string.Concat(Enumerable.Repeat("</i>", iStart - iEnd));
            else
                input = input.RemoveCount("</i>", idiff);

            return input;
        }
    }

    public static class ChatExtensions
    {
        public static string FixChat(this string input)
        {
            var list = new List<char>(); //'i' for <i>, 'b' for <b>, 's' for <size=...>, 'c' for <color=...>
            for (var j = 0; j < input.Length; j++)
            {
                if (input[j] != '<')
                    continue;
                var sub = input.Substring(j);
                if (sub.StartsWith("<i>"))
                    list.Add('i');
                else if (sub.StartsWith("</i>"))
                {
                    if ((list.Count(x => x == 'i') == 0) || ((list.Count > 0) && (list[list.Count - 1] != 'i'))) //no <i> to end lol
                        input = input.Remove(j--, 4);
                    else if ((list.Count > 0) && (list[list.Count - 1] == 'i'))
                        list.RemoveAt(list.Count - 1);
                }
                else if (sub.StartsWith("<b>"))
                    list.Add('b');
                else
                {
                    if (sub.StartsWith("</b>"))
                    {
                        if ((list.Count(x => x == 'b') == 0) || ((list.Count > 0) && (list[list.Count - 1] != 'b')))
                            input = input.Remove(j--, 4);
                        else if ((list.Count > 0) && (list[list.Count - 1] == 'b'))
                            list.RemoveAt(list.Count - 1);
                    }
                    else
                    {
                        if (Regex.IsMatch(sub, @"^<color=?.*?>"))
                            list.Add('c');
                        else if (sub.StartsWith("</color>"))
                        {
                            if ((list.Count(x => x == 'c') == 0) || ((list.Count > 0) && (list[list.Count - 1] != 'c')))
                                input = input.Remove(j--, 8);
                            else if ((list.Count > 0) && (list[list.Count - 1] == 'c'))
                                list.RemoveAt(list.Count - 1);
                        }
                        else
                        {
                            if (Regex.IsMatch(sub, @"^<size=?.*?>"))
                                list.Add('s');
                            else if (sub.StartsWith("</size>"))
                            {
                                if ((list.Count(x => x == 's') == 0) || ((list.Count > 0) && (list[list.Count - 1] != 's')))
                                    input = input.Remove(j--, 7);
                                else if ((list.Count > 0) && (list[list.Count - 1] == 's'))
                                    list.RemoveAt(list.Count - 1);
                            }
                        }
                    }
                }
            }
            if (list.Count <= 0)
                return input;
            for (var i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] == 'c')
                    input += "</color>";
                else if (list[i] == 'i')
                    input += "</i>";
                else if (list[i] == 's')
                    input += "</size>";
                else if (list[i] == 'b')
                    input += "</b>";
            }
            return input;
        }
    }
}