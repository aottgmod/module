﻿using System;
using System.Text;

namespace Modules
{
    [Obsolete]
    class mString
    {
        private StringBuilder _stringBuilder;

        public string CurrentString
        {
            get { return _stringBuilder.ToString(); }
        }

        public int Length
        {
            get { return _stringBuilder.Length; }
        }

        public mString()
        {
            _stringBuilder = new StringBuilder();
        }

        public mString(int capacity)
        {
            _stringBuilder = new StringBuilder(capacity);
        }

        public mString Append(object o)
        {
            if (o is char)
                _stringBuilder.Append((char)o);
            else if ((o as string) != null)
                _stringBuilder.Append((string)o);
            else
                _stringBuilder.Append(o);
            return this;
        }

        public static mString operator +(mString sb, object o)
        {
            if (o is char)
                return sb.Append((char)o);
            if ((o as string) != null)
                sb.Append((string)o);
            return sb.Append(o);
        }

        public static implicit operator string(mString sb)
        {
            return sb.CurrentString;
        }

        public static implicit operator mString(string s)
        {
            return new mString().Append(s);
        }

        public override string ToString()
        {
            return CurrentString;
        }

        public string ToString(int startIndex, int length)
        {
            return _stringBuilder.ToString(startIndex, length);
        }
    }
}