﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using mItems;
using UnityEngine;

namespace Modules
{
    class mGUI : MonoBehaviour
    {
        public static mGUI mgui;

        private static GUIStyle mapObjsStyle;
        private static GUIStyle chatBubbleStyle;

        private static List<GameObject> taggedTitans = new List<GameObject>();
        private static List<GameObject> taggedHeroes = new List<GameObject>();

        private static readonly Color green = new Color(0.4f, 1.0f, 0.4f, .8f);
        private static readonly Color yellow = new Color(1.0f, 1.0f, 0.4f, .8f);
        private static readonly Color red = new Color(1.0f, 0.4f, 0.4f, .8f);
        private static readonly Color shadow_color = new Color(0f, 0f, 0f, 0.5f);
        private static readonly Vector2 shadow = new Vector2(2, 2);

        void Start()
        {
            mgui = this;
            mapObjsStyle = new GUIStyle
            {
                fontSize = 10,
                alignment = TextAnchor.MiddleLeft,
                normal =
                {
                    background = new Texture2D(1, 1)
                }
            };
            for (var x = 0; x < mapObjsStyle.normal.background.width; x++)
            {
                for (var y = 0; y < mapObjsStyle.normal.background.height; y++)
                    mapObjsStyle.normal.background.SetPixel(x, y, new Color(.0f, .0f, .0f, .5f));
            }
            mapObjsStyle.normal.background.Apply();

            chatBubbleStyle = new GUIStyle
            {
                fontSize = 13,
                alignment = TextAnchor.MiddleCenter,
                normal =
                {
                    background = new Texture2D(1, 1),
                    textColor = Color.white
                }
            };
            for (var x = 0; x < chatBubbleStyle.normal.background.width; x++)
            {
                for (var y = 0; y < chatBubbleStyle.normal.background.height; y++)
                    chatBubbleStyle.normal.background.SetPixel(x, y, new Color(.0f, .0f, .0f, .4f));
            }
            chatBubbleStyle.normal.background.Apply();
        }

        void Update()
        {
            ModControl.FPSamount += (Time.deltaTime - ModControl.FPSamount) * 0.1f;
        }

        void OnGUI()
        {
            if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
            {
                showTitans();
                showHumans();

                /*if (FengGameManagerMKII.MKII.chatmessage.Count > 0)
                    chatBubbles();*/
            }
            if ((PhotonNetwork.connectionStateDetailed == PeerState.Joined) || IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                calculateFPS();
        }

        public static Vector2 TextBounds(string text, int font_size = 13)
        {
            return new GUIStyle(GUI.skin.label)
            {
                fontSize = font_size
            }.CalcSize(new GUIContent(text));
        }

        public static void calculateFPS()
        {
            var FPSCount = string.Format("<b>[{0} FPS]</b>", (int)ModControl.fps);
            var color = ModControl.fps >= 35 ? green : ModControl.fps < 20 ? red : yellow;
            GLDraw.DrawShadow(new Rect(Screen.width - TextBounds(FPSCount).x - 3, 70, 60, 20), FPSCount, GUIStyle.none, color, shadow_color, shadow);
        }

        public static void showTitans()
        {
            foreach (var targetObject in GameObject.FindGameObjectsWithTag("titan"))
            {
                if (targetObject == null
                    || ((targetObject.GetComponent<TITAN>() == null || targetObject.GetComponent<TITAN>().hasDie)
                        && (targetObject.GetComponent<FEMALE_TITAN>() == null || targetObject.GetComponent<FEMALE_TITAN>().hasDie)
                        && (targetObject.GetComponent<COLOSSAL_TITAN>() == null || targetObject.GetComponent<COLOSSAL_TITAN>().hasDie)))
                    continue;
                var targetPlayer = targetObject.GetPhotonView().owner;
                var canHook = "<color=#FFBF00>";
                GameObject me = null;
                if (CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object != null)  //fix me
                {
                    me = CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object;
                    if (Vector3.Distance(targetObject.transform.position, me.transform.position) < 120f)
                        canHook = "<color=#00FF00>";
                }
                var nullable1 = new Vector3?(CacheGameObject.Find<Camera>("MainCamera").WorldToScreenPoint(targetObject.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").transform.position));
                if (nullable1.HasValue)
                {
                    var vector3 = nullable1.Value;
                    if (!(vector3.z > 0f))
                        continue;
                    var text = "";
                    var vector2 = GUIUtility.ScreenToGUIPoint(vector3);
                    vector2.y = Screen.height - (vector2.y + 2f);
                    if ((targetObject.name == "Female Titan" && !targetObject.GetComponent<FEMALE_TITAN>().hasDie) ||
                        (targetObject.name == "COLOSSAL_TITAN" && !targetObject.GetComponent<COLOSSAL_TITAN>().hasDie))
                        text = canHook + targetObject.name + "</color>";
                    else if (!targetObject.GetComponent<TITAN>().hasDie)
                    {
                        if (!targetPlayer.isMasterClient)
                        {
                            if (targetPlayer.ID == PhotonNetwork.player.ID)
                                text = "<color=#FFBF00>" + targetObject.name + "</color>";
                            else if (me != null && Vector3.Distance(targetObject.transform.position, me.transform.position) < 120f)
                                    text = "<color=#007F00>[" + targetPlayer.ID + "]</color>";
                            else
                                text = "<color=#FF0000>[" + targetPlayer.ID + "]</color>";
                            if (targetObject.renderer.isVisible)
                                text += "-Visible";
                        }
                        else
                        {
                            //if (targetObject.GetComponent<TITAN>().nonAI).
                            if (targetObject.GetComponent<IN_GAME_MAIN_CAMERA>() != null && (targetObject.GetComponent<IN_GAME_MAIN_CAMERA>().main_object.gameObject.GetPhotonView().owner == targetObject.GetPhotonView().owner))
                                if (me != null && Vector3.Distance(targetObject.transform.position, me.transform.position) < 120f)
                                    text = "<color=#007F00>[" + targetPlayer.ID + "]</color>";
                                else
                                    text = "<color=#FF0000>[" + targetPlayer.ID + "]</color>";
                            else
                                text = canHook + targetObject.name + "</color>";
                        }
                    }
                    GUI.Label(new Rect(vector2.x - TextBounds(text, 10).x / 2f, vector2.y, TextBounds(text, 10).x + 1, 10f), text, mapObjsStyle);
                }
            }
        }

        public static void showHumans()
        {
            foreach (var targetObject in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (targetObject == null || targetObject.GetComponent<HERO>() == null || targetObject.GetComponent<HERO>().hasDied)
                    continue;
                var targetPlayer = targetObject.GetPhotonView().owner;
                var name = targetPlayer.name;
                var color = targetPlayer.stats.hasBoost ? "FF75CF" : "A9D0F5";
                var nullable1 = new Vector3?(CacheGameObject.Find<Camera>("MainCamera").WorldToScreenPoint(targetObject.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head").transform.position));
                if (nullable1.HasValue)
                {
                    var vector3 = nullable1.Value;
                    if (!(vector3.z > 0f))
                        continue;
                    var vector2 = GUIUtility.ScreenToGUIPoint(vector3);
                    vector2.y = Screen.height - (vector2.y + 1f);
                    string text;
                    if (!name.IsNullOrEmpty())
                        text = "<b><color=#FF8000>" + name + "</color></b>" + " <color=#" + color + ">[" + targetPlayer.ID + "]</color>";
                    else
                        text = "<color=#" + color + ">[" + targetPlayer.ID + "]</color>";

                    GUI.Label(new Rect(vector2.x, vector2.y, TextBounds(text, 10).x + 1, 15f), text, mapObjsStyle);
                }
            }
        }
        
        public static void chatBubbles()
        {
            foreach (var player in GameObject.FindGameObjectsWithTag("Player"))
            {
                var playerID = player.GetPhotonView().ownerId;
                if (player != null && FengGameManagerMKII.MKII.chatmessage.ContainsKey(playerID) && FengGameManagerMKII.MKII.chatmessage[playerID].Count > 0)
                {
                    var nullable =
                        new Vector3?(CacheGameObject.Find<Camera>("MainCamera").WorldToScreenPoint(player.transform.position + new Vector3(0f, 4f, 0f)));
                    if (nullable.HasValue)
                    {
                        var value = nullable.Value;
                        if (value.z > 0f)
                        {
                            var msg = string.Join("\n", FengGameManagerMKII.MKII.chatmessage[playerID].ToStringArray());

                            var GUIPoint = GUIUtility.ScreenToGUIPoint(value);
                            GUIPoint.y = Screen.height - GUIPoint.y;
                            var rect = new Rect(GUIPoint.x - TextBounds(msg).x / 2, GUIPoint.y, TextBounds(msg).x + 10, TextBounds(msg).y);
                            GUI.Label(rect, msg, chatBubbleStyle);
                        }
                    }
                }
                if (Time.time - FengGameManagerMKII.MKII.chatmessagecd[playerID] > 3f + .08f * FengGameManagerMKII.MKII.chatmessage[playerID].Count)
                    FengGameManagerMKII.MKII.chatmessage[playerID].Clear(); //mod me remove one line at a time
            }
        }
    }
}
